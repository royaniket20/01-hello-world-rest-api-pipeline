package com.in28minutes.rest.webservices.restfulwebservices;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResponseObject {

	  private String contextPath;
	  private String ip;
	  private int port;
	  private int status;
}
