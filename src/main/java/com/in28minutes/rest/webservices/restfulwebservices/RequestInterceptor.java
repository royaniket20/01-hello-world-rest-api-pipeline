package com.in28minutes.rest.webservices.restfulwebservices;



import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import com.google.gson.Gson;

@Component
public class RequestInterceptor implements HandlerInterceptor {

 
	private static final Logger log = LoggerFactory.getLogger(RequestInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		log.info("----REQUEST COMING IN ------{} at time ->{}",new Gson().toJson(new RequestObject(request.getContextPath(),request.getRemoteAddr(),request.getLocalPort())),LocalDateTime.now());
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		log.info("----REQUEST COMING IN ------{} at time ->{}",new Gson().toJson(new ResponseObject(request.getContextPath(),request.getRemoteAddr(),request.getLocalPort(),response.getStatus())),LocalDateTime.now());
			
	}
	
	

}
