package com.in28minutes.rest.webservices.restfulwebservices;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RequestObject {
	
  private String contextPath;
  private String ip;
  private int port;
  

}
